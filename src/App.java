public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        InvoiceItem invoiceItem1 = new InvoiceItem("HD1108", "Buoi", 50, 50000);
        InvoiceItem invoiceItem2 = new InvoiceItem("HD0306", "Cam", 50, 6000);

        System.out.println(invoiceItem1 + ", " + invoiceItem2);
    }
}
